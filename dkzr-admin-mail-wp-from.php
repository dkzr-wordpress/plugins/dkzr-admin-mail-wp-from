<?php
/**
 * Plugin Name: Use Admin e-mail as WordPress From
 * Version: 1.0.2
 * Plugin URI: https://dkzr.nl/
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/admin-mail-wp-from
 * Author: Joost de Keijzer
 * Author URI: https://dkzr.nl/
 * Description: The default WordPress From e-mail addres is not always what you want. This plugin just replaces it with the admin e-mail addres as set in the General Options.
 */

function dkzr_admin_mail_wp_from( $email ) {
  return get_bloginfo( 'admin_email' );
}
add_filter( 'wp_mail_from', 'dkzr_admin_mail_wp_from' );

function dkzr_admin_mail_wp_from_name( $name ) {
  return 'WordPress admin';
}
add_filter( 'wp_mail_from_name', 'dkzr_admin_mail_wp_from_name' );
